# Observatoriumspraktikum I 2020W und Observatoriumspraktikum II 2021S

[![CI_LaTeX_Compile_Test](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/badges/master/pipeline.svg)](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/pipelines)

## PDF Downloads:

- Template: [here](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/template/template.pdf?job=compile_LaTeX)

- CCD basics: [here](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/CCD_basics/ccd_basics.pdf?job=compile_LaTeX)  
  Autor: Kayran Schmidt

- Spektroskopie: [here](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/spektro/spektroskopie_gruppe8.pdf?job=compile_LaTeX)  
  Autoren: Kayran Schmidt, Alexander Prüller, Ron Malaev

- VLT (Nordkuppel): [here](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/vlt/vlt_protokoll.pdf?job=compile_LaTeX)  
  Autoren: Kayran Schmidt, Alexander Prüller, Ron Malaev, Daniel Karner

- Radio (SRT Sternwarte): [here](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/radio/Radio_ObsPrak.pdf?job=compile_LaTeX)  
  Autoren: Kayran Schmidt

### Alle PDFs im .zip Archiv:
- [link](https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/download?job=compile_LaTeX)

#### Template für Download Links:
`https://gitlab.com/yumasheta/obsprak_ws_2020-ss_2021_protokolle/-/jobs/artifacts/master/raw/<folder>/<file.pdf>?job=compile_LaTeX`

`<folder>`: Pfad relativ zu Repository  
`<file.pdf>`: Name des PDF

* * * *

#### Regeln für neue Ordner

Das .tex LaTeX file muss 1 level unter dem Repository Ordner sein.  
Bsp:
```
obsprak_ws_2020-ss_2021_protokolle/
│   ...
├── semAktForsch            <= OK
│   ├── semaktForsch.bib
│   └── semaktForsch.tex
└── test-template
    └── neuerOrdner         <= nicht OK
        ├── refs.bib
        └── test-template.tex
```
